const Todo = (props) => {
  const handleDelete = (id) => {
    props.setTodos(props.dataTodos.filter((todo) => todo.id != id));
  };
  return (
    <ul className="flex justify-center items-center menu bg-base-100  mt-5 mb-5 w-64 p-2 ">
      <section className="flex font-medium border-b-2 w-fit items-center justify-center mb-2">
        <h3>WHAT U GONNA DO</h3>
      </section>
      {props.dataTodos.map((i) => {
        return (
          <div className="flex justify-center items-center flex-row gap-2 w-full h-full  mb-2">
            <li key={i.id} className=" w-full text-center">
              <a className="inline-block break-all">{i.do}</a>
            </li>
            <button
              onClick={() => handleDelete(i.id)}
              className="btn btn-square btn-outline"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-6 w-6"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M6 18L18 6M6 6l12 12"
                />
              </svg>
            </button>
          </div>
        );
      })}
    </ul>
  );
};

export default Todo;
