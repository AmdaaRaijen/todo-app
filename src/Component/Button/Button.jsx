import React from "react";

const button = () => {
  return (
    <div>
      <button type="submit" className="btn btn-active btn-primary">
        add
      </button>
    </div>
  );
};

export default button;
