import React from "react";

const Header = () => {
  return (
    <div className="bg-base-100 font-bold text-5xl mt-4 border-b-2 pb-4">
      <h1>TODO LIST</h1>
    </div>
  );
};

export default Header;
