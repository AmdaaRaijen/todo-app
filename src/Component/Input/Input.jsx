import React, { useState } from "react";
import Button from "../Button/Button";

const Input = (props) => {
  const [getInput, setInput] = useState("");

  const eventCreateTodo = (todo) => {
    props.setTodos(props.dataTodos.concat(todo));
  };

  const handleChange = (event) => {
    event.preventDefault();
    setInput(event.target.value);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    const newTodo = {
      id: Math.floor(Math.random() * 100) + 1,
      do: getInput,
    };
    if (getInput.trim() != "") {
      eventCreateTodo(newTodo);
    } else {
      console.log("masukkan nilai terlebih dahulu");
    }

    setInput("");
  };

  return (
    <div className="flex flex-row items-center justify-center w-full mt-9 gap-3 form-control ">
      <form className="flex flex-row gap-4 w-max" onSubmit={handleSubmit}>
        <input
          type="text"
          placeholder="Type here"
          className="input input-bordered border-2 w-full max-w-xs"
          value={getInput}
          onChange={handleChange}
        />
        <Button />
      </form>
    </div>
  );
};

export default Input;
