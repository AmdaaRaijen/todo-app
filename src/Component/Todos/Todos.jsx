import React, { useState } from "react";
import Input from "../Input/Input";
import Todo from "../Todo-list/Todo";
import Header from "../Header/Header";

const Todos = () => {
  const [getTodos, setTodos] = useState([]);

  return (
    <div className="border-2 w-96 h-full flex flex-col items-center justify-center rounded-box">
      <Header />
      <Input dataTodos={getTodos} setTodos={setTodos} />
      <div className="flex flex-row items-center justify-center ">
        <Todo dataTodos={getTodos} setTodos={setTodos} />
      </div>
    </div>
  );
};

export default Todos;
