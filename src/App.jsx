import "./App.css";
import "./index.css";
import Input from "./Component/Input/Input";
import Todos from "./Component/Todos/Todos";

function App() {
  return (
    <div className="App flex flex-col items-center justify-center mt-7">
      <Todos />
    </div>
  );
}

export default App;
